import { Component } from '@stencil/core';


@Component({
  tag: 'new-profile',
})
export class NewProfile {

  componentWillLoad() {
    console.log('Loading new profile');
  }

  render() {
    return (
      <div>
        <h1>Hello from new profile</h1>
      </div>
    )
  }
}
